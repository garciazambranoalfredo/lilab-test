import React from "react";

function Card({
  image,
  userName,
  userImage,
  tags,
  text,
  tagFilter,
  likes,
  link,
  date,
  userFilter,
}) {
  return (
    <div className="card my-4 col-10 col-md-6">
      <img src={image} className="card-img-top" alt="..." />
      <div className="card-body">
        <p>{date}</p>
        <h5 className="card-title">{text}</h5>
        <p className="card-text">{`${userName.firstName} ${userName.lastName}`}</p>
        {tags.map((tag) => (
          <a
            href="#"
            className="btn btn-primary me-3 mb-2"
            onClick={() => tagFilter(tag)}
            key={tag}
          >
            {`#${tag}`}
          </a>
        ))}
        <p className="card-text">{`Likes: ${likes}`}</p>
        <a className="card-text" target="_blank" href={link}>
          Ver en Instagram
        </a>
        <a
          href="#"
          className="btn btn-primary ms-3 mb-2"
          onClick={() => userFilter(userName)}
        >
          Ver Comentarios
        </a>
      </div>
    </div>
  );
}

export default Card;

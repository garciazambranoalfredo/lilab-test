import React, { useState, useEffect } from "react";
import axios from "axios";
import Card from "./Card";
import UserModal from "./UserModal";
const BASE_URL = "https://dummyapi.io/data/api";
const APP_ID = "605c8c69386ee076c9bd1e70";

function Posts() {
  const [data, setData] = useState([]);
  const [tag, setTag] = useState(null);
  const [loading, setLoading] = useState(false);
  const [endpoint, setEndpoint] = useState("/post");
  const [showModal, setShowModal] = useState(false);
  const [user, setUserId] = useState(null);

  useEffect(() => {
    setLoading(true);

    if (tag !== null) {
      setEndpoint(`/tag/${tag}/post`);
    } else {
      setEndpoint(`/post`);
    }

    axios
      .get(`${BASE_URL}${endpoint}`, { headers: { "app-id": APP_ID } })
      .then((response) => setData(response.data.data))
      .catch(console.error)
      .finally(() => setLoading(false));
  }, [endpoint, tag]);

  function tagFilter(tag) {
    setTag(tag);
  }

  function userFilter(userId) {
    setShowModal(true);
    setUserId(userId);
  }

  function hideModal() {
    setShowModal(false);
  }

  return (
    <>
      <div className="d-flex align-items-center flex-column container-xl">
        {tag !== null ? (
          <>
            <h4 className="my-4">{`Filtrado de: ${tag}`}</h4>
            <button onClick={() => setTag(null)}>Quitar filtro</button>
          </>
        ) : (
          <h4 className="my-4">Todos los posts!</h4>
        )}
        {loading ? (
          <h4>Cargando...</h4>
        ) : (
          data.map((post) => (
            <Card
              text={post.text}
              image={post.image}
              userName={post.owner}
              tags={post.tags}
              tagFilter={tagFilter}
              userFilter={userFilter}
              likes={post.likes}
              link={post.link}
              date={post.publishDate}
              key={post.text}
            />
          ))
        )}
      </div>

      <UserModal show={showModal} user={user} hideModal={hideModal} />
    </>
  );
}

export default Posts;

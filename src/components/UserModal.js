import React from "react";
import { Modal, Button } from "react-bootstrap";

function UserModal({ show, user, hideModal }) {
  return (
    <Modal
      show={show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">
          {`${user.firstName} ${user.lastName}`}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <img src={user.picture} alt="user" />
        <div>
          <p>{user.email}</p>
          <p>{user.phone}</p>
          <p>{user.gender}</p>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={() => hideModal()}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default UserModal;

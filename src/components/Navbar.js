import React from "react";
import Logo from "../images/logo.svg";

function Navbar() {
  return (
    <div className="bg-light shadow position-sticky">
      <div className="navbar d-flex justify-content-center">
        <img src={Logo} alt="team pets logo" />
      </div>
    </div>
  );
}

export default Navbar;

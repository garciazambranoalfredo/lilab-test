import "./styles/bootstrap.min.css";
import "./styles/App.css";
import Navbar from "./components/Navbar";
import Posts from "./components/Posts";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Posts />
    </div>
  );
}

export default App;
